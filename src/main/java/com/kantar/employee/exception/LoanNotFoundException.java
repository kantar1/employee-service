package com.kantar.employee.exception;

public class LoanNotFoundException extends Exception {
	
	public LoanNotFoundException(String msg) {
		super(msg);
	}

}
