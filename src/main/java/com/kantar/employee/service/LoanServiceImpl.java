package com.kantar.employee.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kantar.employee.entity.Loan;
import com.kantar.employee.exception.LoanAlreadyExistsException;
import com.kantar.employee.exception.LoanNotFoundException;
import com.kantar.employee.repository.LoanRepository;

@Service
public class LoanServiceImpl implements LoanService {
	
	@Autowired
	LoanRepository loanRepository;


	@Override
	public Loan save(Loan loan) throws LoanAlreadyExistsException {
		Optional<Loan> findByName = this.loanRepository.findByName(loan.getName());
		if (findByName.isPresent())
			throw new LoanAlreadyExistsException("Loan Already Exists With this Name :" + loan.getName());
		else
			return this.loanRepository.save(loan);
	}

	@Override
	public List<Loan> getAll() {
		return this.loanRepository.findAll();
	}

	@Override
	public Loan getLoanById(int id) throws LoanNotFoundException {
		Optional<Loan> findById = this.loanRepository.findById(id);
		if (!findById.isPresent())
			throw new LoanNotFoundException("Loan Not Found With this Id: " + id );		
		return this.loanRepository.findById(id).get();
	
	}

	@Override
	public Loan update(Loan loan) throws LoanNotFoundException {
		Optional<Loan> findById = this.loanRepository.findById(loan.getId());
		if (!findById.isPresent())
			throw new LoanNotFoundException("Loan Not Found With this Id: " + loan.getId() );		
		
		return this.loanRepository.save(loan);
	}

	@Override
	public boolean delete(int id) throws LoanNotFoundException {
		Optional<Loan> findById = this.loanRepository.findById(id);
		if (!findById.isPresent())
			throw new LoanNotFoundException("Loan Not Found With this Id: " + id );			
		this.loanRepository.delete(findById.get());
		return true;
}

}
