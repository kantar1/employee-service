package com.kantar.employee.service;

import java.util.List;

import com.kantar.employee.entity.Loan;
import com.kantar.employee.exception.LoanAlreadyExistsException;
import com.kantar.employee.exception.LoanNotFoundException;

public interface LoanService {

	public Loan save(Loan loan) throws LoanAlreadyExistsException;

	public List<Loan> getAll();

	public Loan getLoanById(int id) throws LoanNotFoundException;

	public Loan update(Loan loan) throws LoanNotFoundException;

	public boolean delete(int id) throws LoanNotFoundException;

}
