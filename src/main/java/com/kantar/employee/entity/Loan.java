package com.kantar.employee.entity;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "loan_details")
public class Loan {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "loan_id")
	private int id;
	
	@Column(name = "loan_name")
	private String name;
	
	@Column(name = "loan_interest_rate")
	private float interestRate;
	
	@Column(name = "loan_amount")
	private double loanAmount;
	/*
	@ManyToMany(mappedBy = "loans")
	@JsonIgnore
    private List<Employee> employees;
 */
}
