package com.kantar.employee.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.kantar.employee.entity.Employee;
import com.kantar.employee.exception.EmployeeAlreadyExistsException;
import com.kantar.employee.exception.EmployeeNotFoundException;
import com.kantar.employee.service.EmployeeService;

import jakarta.validation.Valid;

@RestController
@RequestMapping("api/v1/employees")
public class EmployeeController {

	@Autowired
	EmployeeService empService;

	@GetMapping
	public ResponseEntity<?> getAll() {
		return ResponseEntity.ok(this.empService.getAll());
	}

	@PostMapping
	public ResponseEntity<?> save(@Valid @RequestBody Employee employee) throws EmployeeAlreadyExistsException {
		Employee createdEmployee;
		try {
			createdEmployee = this.empService.save(employee);
		} catch (EmployeeAlreadyExistsException e) {
			throw e;
		}
		return ResponseEntity.status(HttpStatus.CREATED).body(createdEmployee);

	}

	/*
	 * @GetMapping("/employee") public String getEmployee(@RequestParam int id) {
	 * System.out.println("id : " + id); return "employee with id: " + id +
	 * "is returned successfully"; }
	 */

	@GetMapping("/{id}")
	public ResponseEntity<?> getEmployeeById(@PathVariable int id) throws EmployeeNotFoundException{
		Employee employee;
		try {
			employee = this.empService.getEmployeeById(id);
		} catch (EmployeeNotFoundException e) {
			throw e;
		}
		return ResponseEntity.ok(employee);
	}

	@PutMapping
	public ResponseEntity<?> update(@RequestBody Employee employee) throws EmployeeNotFoundException {
		Employee updatedEmployee;
		try {
			updatedEmployee = this.empService.update(employee);
		} catch (EmployeeNotFoundException e) {
			throw e;
		}
		return ResponseEntity.ok(updatedEmployee);
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<?> delete(@PathVariable int id) throws EmployeeNotFoundException{
		try {
			this.empService.delete(id);
		} catch (EmployeeNotFoundException e) {
			throw e;
		}
		return ResponseEntity.ok(true);
	}

}
