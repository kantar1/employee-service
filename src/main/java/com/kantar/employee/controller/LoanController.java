package com.kantar.employee.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.kantar.employee.entity.Loan;
import com.kantar.employee.exception.LoanAlreadyExistsException;
import com.kantar.employee.exception.LoanNotFoundException;
import com.kantar.employee.service.LoanService;

@RestController
@RequestMapping("api/v1/loans")
public class LoanController {

	@Autowired
	LoanService loanService;

	@GetMapping
	public ResponseEntity<?> getAll() {
		return ResponseEntity.ok(this.loanService.getAll());
	}

	@PostMapping
	public ResponseEntity<?> save(@RequestBody Loan loan) throws LoanAlreadyExistsException {
		Loan createdLoan;
		try {
			createdLoan = this.loanService.save(loan);
		} catch (LoanAlreadyExistsException e) {
			throw e;
		}
		return ResponseEntity.status(HttpStatus.CREATED).body(createdLoan);

	}

	/*
	 * @GetMapping("/employee") public String getEmployee(@RequestParam int id) {
	 * System.out.println("id : " + id); return "employee with id: " + id +
	 * "is returned successfully"; }
	 */

	@GetMapping("/{id}")
	public ResponseEntity<?> getLoanById(@PathVariable int id) throws LoanNotFoundException{
		Loan loan;
		try {
			loan = this.loanService.getLoanById(id);
		} catch (LoanNotFoundException e) {
			throw e;
		}
		return ResponseEntity.ok(loan);
	}

	@PutMapping
	public ResponseEntity<?> update(@RequestBody Loan loan) throws LoanNotFoundException {
		Loan updatedLoan;
		try {
			updatedLoan = this.loanService.update(loan);
		} catch (LoanNotFoundException e) {
			throw e;
		}
		return ResponseEntity.ok(updatedLoan);
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<?> delete(@PathVariable int id) throws LoanNotFoundException{
		try {
			this.loanService.delete(id);
		} catch (LoanNotFoundException e) {
			throw e;
		}
		return ResponseEntity.ok(true);
	}
}
